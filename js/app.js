
// шапка

const $header = document.querySelector('.header'),
$headerLang = document.querySelectorAll('.header-lang');


window.addEventListener('scroll' , (e) => {
    let $scrl = window.scrollY;

    if ($scrl > 10) {
        $header.classList.add('active');
    }
    if ($scrl < 10) {
        $header.classList.remove('active');
    }
    
});

$headerLang.forEach(item => {
    item.addEventListener('click', e => {
        e.preventDefault();

        const $headerLangDrop = item.querySelector('.header-lang-drop') ;

        if (e.target.closest('.header-lang-drop')) {
            item.classList.add('active');
            $headerLangDrop.classList.add('active');
        } else {
            item.classList.toggle('active');
            $headerLangDrop.classList.toggle('active');
        }
        
    });
})

// шапка моб меню

const $modalMenu = document.querySelector('.modal-menu'),
$menuOpenM = document.querySelector('.menu-open-m');


if ($menuOpenM) {
    $menuOpenM.addEventListener('click', e => {
        e.preventDefault();
        if ($menuOpenM.getAttribute('active')) {
            $menuOpenM.classList.remove('active');
            $menuOpenM.removeAttribute('active');
            $modalMenu.classList.remove('active');
            document.body.classList.remove('active');
        } else {
            $menuOpenM.classList.add('active');
            $menuOpenM.setAttribute('active', 'active');
            $modalMenu.classList.add('active');
            document.body.classList.add('active');
        }
        
    });
}

// faq 

const $faqBlock = document.querySelector('.faq-block');

if ($faqBlock) {
    $faqBlock.addEventListener('click', e => {
        e.preventDefault();
        const $target = e.target;
        const $faqItem = $target.closest('.faq-block-item');
        const $faqItemTop = $target.closest('.top');
        const $faqItemAll = $faqBlock.querySelectorAll('.faq-block-item');
    
        if ($faqItem) {
            if ($faqItem.getAttribute('active') && $faqItemTop) {
    
                $faqItemAll.forEach(item => {
                    item.classList.remove('active');
                    item.removeAttribute('active');
                });
                $faqItem.classList.remove('active');
                $faqItem.removeAttribute('active');
            } else {
                
                $faqItemAll.forEach(item => {
                    item.classList.remove('active');
                    item.removeAttribute('active');
                });
                $faqItem.setAttribute('active', 'active');
                $faqItem.classList.add('active');
               
            }
            
        } 
    
    });
    
}

// показать весь текст

const $boxVisDescp = document.querySelectorAll('.box-vis-descp');

if ($boxVisDescp) {
    $boxVisDescp.forEach(item => {

        const $read = item.querySelector('.read-more');
        const $subtext = item.querySelector('.subtitle');
    
        $read.addEventListener('click', e => {
            e.preventDefault();
            $subtext.classList.toggle('active');
        });
    });
}

// таймер отсчета ( бонус )

const $timerShowHour = document.querySelector('.btn-timer .hour '),
$timerShowMinuts = document.querySelector('.btn-timer .minuts '),
$timerShowSeconds = document.querySelector('.btn-timer .seconts');

let $timeMinut = 3759.8 * 19; //* время таймера

if ($timerShowSeconds) {

    window.onload = function() {
    
        timer = setInterval(function () {
            seconds = $timeMinut%60
            minutes = $timeMinut/60%60
            hour = $timeMinut/60/60%60
            if (!$timeMinut <= 0) {
                let strTimerHour = `${Math.trunc(hour)}`;
                let strTimerMinuts = `${Math.trunc(minutes)}`;
                let strTimerSeconds = `${Math.trunc(seconds)}`;
                
                $timerShowHour.textContent = strTimerHour;
                $timerShowMinuts.textContent = strTimerMinuts;
                $timerShowSeconds.textContent = strTimerSeconds;
            
            }
            --$timeMinut;
            
        }, 1000);
    };
}


// анимации вычисляем положение

const $addAnimAll = document.querySelector('.addAnimAll');

if ($addAnimAll) {
    
    var Visible = function (target) {
        // Все позиции элемента
        var targetPosition = {
            top: window.pageYOffset + target.getBoundingClientRect().top,
            left: window.pageXOffset + target.getBoundingClientRect().left,
            right: window.pageXOffset + target.getBoundingClientRect().right,
            bottom: window.pageYOffset + target.getBoundingClientRect().bottom
            },
            // Получаем позиции окна
            windowPosition = {
            top: window.pageYOffset,
            left: window.pageXOffset,
            right: window.pageXOffset + document.documentElement.clientWidth,
            bottom: window.pageYOffset + document.documentElement.clientHeight
            };

        if (targetPosition.bottom > windowPosition.top && // Если позиция нижней части элемента больше позиции верхней чайти окна, то элемент виден сверху
            targetPosition.top < windowPosition.bottom && // Если позиция верхней части элемента меньше позиции нижней чайти окна, то элемент виден снизу
            targetPosition.right > windowPosition.left && // Если позиция правой стороны элемента больше позиции левой части окна, то элемент виден слева
            targetPosition.left < windowPosition.right) { // Если позиция левой стороны элемента меньше позиции правой чайти окна, то элемент виден справа
           
            $addAnimAll.querySelectorAll('.animate__animated').forEach(item => {
                item.classList.add('animate__bounceInUp')
            });
            
        } 
    };

    // Запускаем функцию при прокрутке страницы
    window.addEventListener('scroll', function() {
        Visible ($addAnimAll);
    });

    // А также запустим функцию сразу. А то вдруг, элемент изначально видно
    Visible ($addAnimAll);


}

// показ слотов 

const $slotsViewJs = document.querySelector('.slots-view-js'),
$slotsViewJsItems = [...document.querySelectorAll('.slots-view-js .row-slots-item')],
$readMoreSlotsJs = document.querySelector('.read-more-slots-js');

if ($slotsViewJs) {
    const slotsView = (maxCount) => {
        
        for (let i = 0; i < $slotsViewJsItems.length; i++) {
            $slotsViewJsItems[i].classList.remove('disable');

            for (let j = maxCount; j < $slotsViewJsItems.length; j++) {
                $slotsViewJsItems[j].classList.add('disable');
                
            }
            
            
        }

    }

    slotsView(28);

    $readMoreSlotsJs.addEventListener('click', e => {
        e.preventDefault();

        if ($slotsViewJs.getAttribute('active')) {
            slotsView(28);
            $slotsViewJs.removeAttribute('active');
        } else {
            slotsView(100000000);
            $slotsViewJs.setAttribute('active', 'active');
        }
       
    });

}


// слайдер

new Swiper('.swiper-container', {
    loop: false,
    slidesPerView: 3,
    grabCursor: true,
    centeredSlides: true,
    slideToClickedSlide: true,
    
});